/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory;

/**
 *
 * @author Adeel Khilji
 */
public class Inventory 
{
    Item item;
    
    public static Item[] inventory = new Item[100];
    private int itemCounter = 0; // counts numbers of items in the inventory.
    private int quantity;
    
    public void addItem(Item newItem) {
        inventory[itemCounter] = newItem;
        itemCounter++;
        quantity++;
        
    }

    public void printInventory() {
        for (int i = 0; i < itemCounter; i++) {
            System.out.println("ID: " + inventory[i].getId()
                    + "\t Name: " + inventory[i].getName()
                    + "\t Quantity:" + inventory[i].getQuantity());
        }
    }

    public int getItemQuantity(int ID) {
        int temp = 0;
        for (int j = 0; j < itemCounter; j++) {
            if (inventory[j].itemID.getId() == ID) {
                temp = inventory[j].qty.getQty();
                break;
            }
        }
        return temp;
    }
    
}
