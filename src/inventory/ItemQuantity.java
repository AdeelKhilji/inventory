/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory;

/**
 *
 * @author Adeel Khilji
 */
public class ItemQuantity 
{
    private int quantity;
    
    public ItemQuantity(int quantity) 
    {
        this.quantity = quantity;
    }
    
    public int getQty()
    {
        return this.quantity;
    }
}
