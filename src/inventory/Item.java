package inventory;

/**
 * A class to model items stored in the inventory of a Warehouse. For simplicity, let's assume each item in the
 * inventory has a name, quantity and a unique ID. The system allows to add new items, find quantity by item's ID and
 * print full inventory. Use this code to answer Q#16 and Q#17 of Part B.
 *
 * @author Adeel Khilji
 */
public class Item {

    
    ItemId itemID;
    ItemName name;
    ItemQuantity qty;
    
    public Item(ItemId itemID, ItemQuantity qty, ItemName name) {
        this.itemID = itemID;
        this.qty = qty;
        this.name = name;
    }
    
    public ItemId getId()
    {
        return this.itemID;
    }
    
    public ItemName getName()
    {
        return this.name;
    }
    public ItemQuantity getQuantity()
    {
        return this.qty;
    }
}
