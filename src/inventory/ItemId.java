/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inventory;

/**
 *
 * @author Adeel Khilji
 */
public class ItemId 
{
    private int itemID;
    
    public ItemId(int itemID) 
    {
        this.itemID = itemID;
    }
    
    public int getId()
    {
        return this.itemID;
    }
}
