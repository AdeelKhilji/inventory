package inventory;

import java.util.Scanner;

/**
 * Simulator to add items, print inventory and to search item's quantity.
 *
 * @author Adeel Khilji
 */
public class InventorySystem {

    public static void main(String[] args) {
        
        
        ItemId Id = new ItemId(1);

        Scanner input = new Scanner(System.in);
        
        System.out.println("Enter name of the item to add:");
        String name = input.nextLine();
        
        System.out.println("Enter the quantity:");
        int quantity = input.nextInt();
        ItemName name = new ItemName(name);
        ItemQuantity qty = new ItemQuantity(qty);
        Item item1 = new Item();
        Item item2 = new Item(++item1.itemID, quantity, name);
        item1.addItem(item2);

        /**
         * Add two more items.
         */
        item1.itemID;
        Item item3 = new Item(, 5, "ET-2750 printer");
        item1.addItem(item3);

        Item item4 = new Item(++item1.itemID, 10, "MX-34 laptops");
        item1.addItem(item4);

        /**
         * Print the inventory.
         */
        item1.printInventory();

        /**
         * Find quantity of a particular item in the inventory.
         */
        System.out.println("Enter ID of the item whose quantity you want to find:");
        int temp_ID = input.nextInt();
        System.out.println("Item's quantity is: " + item1.itemID);

    }

}
